var homeApp=angular.module('homeApp',['ngRoute']);

homeApp.config(function($routeProvider){
	$routeProvider

		// home page routing
		.when('/',{
			templateUrl: 'views/home.html',
			controller: 'MainCtrl'
		})

		// route for the about page
		.when('/about', {
			templateUrl:'views/about.html',
			controller:'AboutCtrl'
		})

		//route for the contact page
		.when('/contact',{
			templateUrl:'views/contact.html',
			controller:'ContactCtrl'
		})

		//route for the coaching page
		.when('/coaching',{
			templateUrl:'views/coaching.html',
			controller:'CoachingCtrl'
		})
		.when('/projects',{
			templateUrl:'views/projects.html',
			controller:'ProjectsCtrl'
		});
});


